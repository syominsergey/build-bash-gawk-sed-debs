#!/usr/bin/env bash

# print bash instructions to stdout
set -o xtrace
COMMAND_TO_RUN="${*:1}"
TRY_NUM=1
MAX_TRIES=5
while :
do
    if [ $TRY_NUM -le $MAX_TRIES ]; then
      echo "Attempt to run command number $TRY_NUM"
    else
      echo "All $MAX_TRIES attempts to run command were unsuccessful. Now stopping with error"
      exit 1
    fi
    $COMMAND_TO_RUN
    EXIT_CODE=$?
    echo "Exit code of run attempt: $EXIT_CODE"
    if [ $EXIT_CODE -eq 0 ]; then
      exit 0
    fi
    TRY_NUM=$(( TRY_NUM + 1 ))
done