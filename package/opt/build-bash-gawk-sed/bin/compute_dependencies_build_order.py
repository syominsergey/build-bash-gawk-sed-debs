#!/bin/env python3
import subprocess
import sys
import os
from typing import List, Optional
from typing import Set
from functools import lru_cache


class SubprocessError(Exception):
    def __init__(self, cmd: List[str], return_code: int, output: str):
        super().__init__("Error when running subprocess: cmd {}, return code {}, output {}"
                         .format(cmd, return_code, output))


def run_subprocess(cmd: List[str]) -> str:
    result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    output = result.stdout.decode("utf-8")
    if result.returncode != 0:
        raise SubprocessError(cmd, result.returncode, output)
    return output


def truncate_str_up_to_substr(src: str, substr: str) -> str:
    pos = src.find(substr)
    if pos != -1:
        src = src[:pos]
    return src


def extract_dependencies_from_line(line: str) -> List[str]:
    deps = line.split(",")
    for i in range(len(deps)):
        # Бывают строки вида "libselinux-dev [linux-any]". Не рассматриваем платформу для простоты.
        deps[i] = truncate_str_up_to_substr(deps[i], "[")
        # Бывают строки вида "debhelper (>= 10)". Не рассматриваем версию для простоты.
        deps[i] = truncate_str_up_to_substr(deps[i], "(")
        # Бывают строки вида "dbus <!nocheck>". Не рассматриваем дополнительные ограничения для простоты.
        deps[i] = truncate_str_up_to_substr(deps[i], "<")
        # Бывают строки вида "python3-all:any". Отрезаем то, что после двоеточия.
        deps[i] = truncate_str_up_to_substr(deps[i], ":")
        # Бывают строки вида "mawk | awk". Рассматриваем для простоты первую альтернативу.
        deps[i] = truncate_str_up_to_substr(deps[i], "|")
        deps[i] = deps[i].strip()
    return deps


def find_num_of_line_starting_with_prefix(lines: List[str], prefix: str) -> int:
    for num, line in enumerate(lines):
        if line.startswith(prefix):
            return num
    return -1


def get_line_with_prefix_truncated(package_src_desc_lines: List[str], prefix: str) -> Optional[str]:
    pos = find_num_of_line_starting_with_prefix(package_src_desc_lines, prefix)
    if pos == -1:
        return None
    return package_src_desc_lines[pos][len(prefix):]


class UnableToLocatePackage(Exception):
    def __init__(self, package_name: str):
        super().__init__("Unable to locate package " + package_name)


def check_package_found(package_name: str, output: str):
    if "Unable to locate package" in output:
        raise UnableToLocatePackage(package_name)


def get_package_src_description(package_name: str) -> str:
    output = run_subprocess(["apt-cache", "showsrc", package_name])
    check_package_found(package_name, output)
    return output


def find_real_packages_provides_virtual(virtual_package_name: str) -> Set[str]:
    output = run_subprocess(["apt-cache", "showpkg", virtual_package_name])
    check_package_found(virtual_package_name, output)
    lines = output.split("\n")
    pos = find_num_of_line_starting_with_prefix(lines, "Reverse Provides:")
    if pos == -1:
        return set()
    result = set()
    for line in lines[pos + 1:]:
        line = truncate_str_up_to_substr(line, " ")
        if line:
            result.add(line)
    result = set(sorted(result))
    return result


def get_real_package_name(package_name: str) -> str:
    try:
        get_package_src_description(package_name)
    except UnableToLocatePackage:
        try:
            providing_packages = find_real_packages_provides_virtual(package_name)
        except UnableToLocatePackage:
            providing_packages = set()
        if not providing_packages:
            raise UnableToLocatePackage(package_name)
        first_package = next(iter(providing_packages))
        return first_package
    return package_name


def list_real_build_dependencies(package_name: str) -> Set[str]:
    package_src_desc_lines = get_package_src_description(package_name).split("\n")
    dependencies = set()
    for property in ["Build-Depends", "Build-Depends-Indep", "Build-Depends-Arch"]:
        build_depends_line = get_line_with_prefix_truncated(package_src_desc_lines, property + ":")
        if not build_depends_line:
            continue
        for dependency in extract_dependencies_from_line(build_depends_line):
            dependencies.add(dependency)
    result = set()
    for dependency in dependencies:
        try:
            real_package_name = get_real_package_name(dependency)
        except UnableToLocatePackage:
            # случай, когда в пакетах-зависимостях есть пакет из другой системы, типа из freebsd, например
            # по-хорошему он должен был бы отсеяться в extract_dependencies_from_line, но там не реализована фильтрация
            # по системам, так что отсеиваем здесь
            continue
        result.add(real_package_name)
    return result


@lru_cache
def list_real_build_dependencies_cached(package_name: str) -> Set[str]:
    return list_real_build_dependencies(package_name)


def read_target_packages() -> Set[str]:
    result = set()
    for line in sys.stdin:
        result.add(line[:-1])
    return result


def validate_result(result: List[str], target_packages: Set[str]):
    validation_mode = os.environ.get("VALIDATE_RESULT", "target")
    if validation_mode == "off":
        return
    validate_all = validation_mode == "all"
    packages_left_to_build = set()
    for package in result:
        packages_left_to_build.add(package)
    for package in result:
        if validate_all or (package in target_packages):
            deps = list_real_build_dependencies_cached(package)
            for dep in deps:
                if dep in packages_left_to_build:
                    raise Exception("trying to build package {} before building its dependency {}".format(package, dep))
        packages_left_to_build.remove(package)


if __name__ == "__main__":
    target_packages = read_target_packages()
    # словарь с ключом, которым является имя пакета для сборки, а значением - количество пакетов для сборки, которые
    # на него ссылаются как на сборочную зависимость. Рассматривается только множество пакетов-зависимостей.
    deps_usage_count_dict = {}
    for package in sorted(target_packages):
        deps = list_real_build_dependencies_cached(package)
        for dep in deps:
            deps_usage_count_dict[dep] = 0
    for package in sorted(deps_usage_count_dict.keys()):
        deps = list_real_build_dependencies_cached(package)
        for dep in deps:
            if dep in deps_usage_count_dict:
                deps_usage_count_dict[dep] += 1
    # сортируем сначала по убыванию метрики, а в случае совпадения значения - лексикографически по возрастанию
    # (это нужно для детерминированности выдачи)
    result = [p for p in sorted(deps_usage_count_dict.keys(), key=lambda p: (-deps_usage_count_dict[p], p))]
    for package in sorted(target_packages):
        result.append(package)
    validate_result(result, target_packages)
    for package in result:
        print(package)
