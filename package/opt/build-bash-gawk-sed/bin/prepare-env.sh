#!/usr/bin/env bash

# stop script on first error - "fail fast"
set -e
# print bash instructions to stdout
set -o xtrace

cd /root

# setup locales
source ./set-locale.sh

# add local repository
sed -i '1s/^/deb [trusted=yes] file:\/\/\/root\/local-repo bullseye main\n/' /etc/apt/sources.list
# add repository with sources
echo "deb-src https://mirror.yandex.ru/debian bullseye main" >> /etc/apt/sources.list

# get info about new packages
./run-with-tries.sh apt update
# upgrade to newest packages
./run-with-tries.sh apt -y upgrade
