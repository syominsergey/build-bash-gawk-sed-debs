#!/usr/bin/env bash

# stop script on first error - "fail fast"
set -e
# print bash instructions to stdout
set -o xtrace

cd /root
# setup locales
source ./set-locale.sh

./run-with-tries.sh apt -y install python3

cat ./packages-to-build.txt | ./compute_dependencies_build_order.py > packages-build-order.txt

echo "List of packages to build has $(cat packages-build-order.txt | wc -l) entities:"
cat packages-build-order.txt
