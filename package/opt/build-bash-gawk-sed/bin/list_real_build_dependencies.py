#!/bin/env python3
import sys
from compute_dependencies_build_order import list_real_build_dependencies


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Need 1 positional argument:")
        print("- package name")
        sys.exit(1)
    package_name = sys.argv[1]
    real_build_dependencies = list_real_build_dependencies(package_name)
    for dependency in sorted(real_build_dependencies):
        print(dependency)
