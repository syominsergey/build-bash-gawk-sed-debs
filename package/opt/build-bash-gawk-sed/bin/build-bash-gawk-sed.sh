#!/usr/bin/env bash

# stop script on first error - "fail fast"
set -e
# print bash instructions to stdout
set -o xtrace

# get dir where script is located. See https://stackoverflow.com/a/246128
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# check target dir defined in env var
if [ -z "$TARGET_DIR" ]; then
    # if not defined - show error and exit
    echo "Target dir was not set. Use env var TARGET_DIR for setting it."
    exit 1
fi
# check that target dir is not a simple file
if [ -f "$TARGET_DIR" ]; then
    echo "Some file with name $TARGET_DIR exists, but it is not a directory. Please use empty directory as target."
    exit 1
fi
# check if target dir exists
if [ -d "$TARGET_DIR" ]; then
    # check if target dir is not empty
    if [ ! -z "$(ls -A $TARGET_DIR)" ]; then
        echo "WARNING! Some directory with name $TARGET_DIR selected as target dir exists and is not empty. Use it at your own risk!"
    fi
else
    # create target dir if not exists
    mkdir -p "$TARGET_DIR"
fi

# check working dir defined in env var
if [ -z "$WORKING_DIR" ]; then
    # if not defined - create new temporary dir and print path
    WORKING_DIR="$(mktemp -d)"
    echo "Working dir was not explicitly set with var WORKING_DIR, so will use new temporary dir $WORKING_DIR."
else
    # if working dir defined - check if it a file
    if [ -f "$WORKING_DIR" ]; then
        # show error if warking dir points to file and exit
        echo "Some file exists on path $WORKING_DIR. Please use another path for working dir."
        exit 1
    fi
    # check if working dir already exists
    if [ -d "$WORKING_DIR" ]; then
        # if working dir exists - check if it is empty
        if [ ! -z "$(ls -A $WORKING_DIR)" ]; then
            # if working dir is not empty - show warning
            echo "WARNING! Some directory with name $WORKING_DIR selected as working dir exists and is not empty. Use it at your own risk!"
        fi
    else
        # if working dir does not exsist - create it
        mkdir -p "$WORKING_DIR"
    fi
fi

# check dir for logs defined in env var
if [ -z "$LOGS_DIR" ]; then
    # if not defined - show error and exit
    echo "Logs dir was not set. Use env var LOGS_DIR for setting it."
    exit 1
fi
# check dir for logs points to file
if [ -f "$LOGS_DIR" ]; then
    # if dir for logs points to file - show error and exit
    echo "Some file with name $LOGS_DIR exists, but it is not a directory. Please use empty directory as logs dir."
    exit 1
fi
# check if dir for logs exists
if [ -d "$LOGS_DIR" ]; then
    # if dir for logs exists - check is it empty
    if [ ! -z "$(ls -A $LOGS_DIR)" ]; then
        # if die for logs not empty - show warning
        echo "WARNING! Some directory with name $LOGS_DIR selected as logs dir exists and is not empty. Use it at your own risk!"
    fi
else
    # if dir for logs does not exist - create it
    mkdir -p "$LOGS_DIR"
fi

# setup local repository
mkdir -p "$TARGET_DIR/conf"
cp "$SCRIPT_DIR/../etc/reprepro-distributions.conf" "$TARGET_DIR/conf/distributions"
reprepro -b "$TARGET_DIR" clearvanished
reprepro -b "$TARGET_DIR" export

# dir name for base environment used for creation other environments. It speeds up their creation becouse there is no need to do all work from the beginning
ROOT_BACKUP="$WORKING_DIR/root.bak"
# dir name for current building environment
ROOT_CURRENT="$WORKING_DIR/root"

# create dir for base environment
mkdir -p "$ROOT_BACKUP"
# prepare base environment using debootstrap for current architecture
"$SCRIPT_DIR"/run-with-tries.sh /usr/sbin/debootstrap --arch $(dpkg --print-architecture) bullseye "$ROOT_BACKUP" https://mirror.yandex.ru/debian/ |& tee "$LOGS_DIR/prepare-env.log"
# copy auxillary script for prepartion of environment into base environment
cp "$SCRIPT_DIR/set-locale.sh" "$ROOT_BACKUP/root"
cp "$SCRIPT_DIR/prepare-env.sh" "$ROOT_BACKUP/root"
cp "$SCRIPT_DIR/build-package.sh" "$ROOT_BACKUP/root"
cp "$SCRIPT_DIR/run-with-tries.sh" "$ROOT_BACKUP/root"

mkdir "$ROOT_BACKUP/root/local-repo"
mount --bind "$TARGET_DIR" "$ROOT_BACKUP/root/local-repo"
mount proc "$ROOT_BACKUP/proc" -t proc
mount sysfs "$ROOT_BACKUP/sys" -t sysfs
# chroot into base environment and run preparation script
chroot "$ROOT_BACKUP" /root/prepare-env.sh |& tee -a "$LOGS_DIR/prepare-env.log"
umount "$ROOT_BACKUP/sys"
umount "$ROOT_BACKUP/proc"
umount "$ROOT_BACKUP/root/local-repo"

cp -r "$ROOT_BACKUP" "$ROOT_CURRENT"
cp "$SCRIPT_DIR/compute_dependencies_build_order.sh" "$ROOT_CURRENT/root"
cp "$SCRIPT_DIR/compute_dependencies_build_order.py" "$ROOT_CURRENT/root"
cp "$SCRIPT_DIR/../etc/packages-to-build.txt" "$ROOT_CURRENT/root"
mount --bind "$TARGET_DIR" "$ROOT_CURRENT/root/local-repo"
mount proc "$ROOT_CURRENT/proc" -t proc
mount sysfs "$ROOT_CURRENT/sys" -t sysfs
chroot "$ROOT_CURRENT" /root/compute_dependencies_build_order.sh |& tee -a "$LOGS_DIR/compute_dependencies_build_order.log"
umount "$ROOT_CURRENT/sys"
umount "$ROOT_CURRENT/proc"
umount "$ROOT_CURRENT/root/local-repo"
mv "$ROOT_CURRENT/root/packages-build-order.txt" "$WORKING_DIR"
rm -r "$ROOT_CURRENT"

# itarate over lines in file containing list of packages to build
PACKAGE_NUM=1
while IFS= read -r PACKAGE
do
    # create current environment by means of cloning base environment
    cp -r "$ROOT_BACKUP" "$ROOT_CURRENT"

    mount --bind "$TARGET_DIR" "$ROOT_CURRENT/root/local-repo"
    # mount auxillary file systems. Without them bash could not be built because of error "needed to have HAVE_DEV_STDIN defined"
    # there file systems are listed in manual https://wiki.debian.org/ru/Debootstrap
    mount proc "$ROOT_CURRENT/proc" -t proc
    mount sysfs "$ROOT_CURRENT/sys" -t sysfs

    echo "building $PACKAGE"
    # chroot into build environment anr run script for building certain package
    chroot "$ROOT_CURRENT" /root/build-package.sh "$PACKAGE" |& tee "$LOGS_DIR/$PACKAGE_NUM-build-$PACKAGE.log"
    # move resulting deb files into repository
    reprepro -b "$TARGET_DIR" includedeb bullseye "$ROOT_CURRENT/root/$PACKAGE"/*.deb

    # unmount auxillary file systems
    umount "$ROOT_CURRENT/sys"
    umount "$ROOT_CURRENT/proc"
    umount "$ROOT_CURRENT/root/local-repo"
    # remove current build environment because we need to build every package in its own clean environment
    rm -r "$ROOT_CURRENT"
    PACKAGE_NUM=$(( PACKAGE_NUM + 1 ))
done < "$WORKING_DIR/packages-build-order.txt"

# remove dir with temporary files
rm -r "$WORKING_DIR"
