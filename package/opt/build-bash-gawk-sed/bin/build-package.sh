#!/bin/env bash

# stop script on first error - "fail fast"
set -e
# print bash instructions to stdout
set -o xtrace

PACKAGE=$1

cd /root
# setup locales
source ./set-locale.sh

./run-with-tries.sh apt update

# go into working directory for building packages
mkdir "$PACKAGE"
cd "$PACKAGE"
# install build dependencies
../run-with-tries.sh apt -y build-dep "$PACKAGE"
../run-with-tries.sh apt -y source "$PACKAGE"
cd "$(ls -d */)"
DEB_BUILD_OPTIONS=nocheck dpkg-buildpackage -b -uc -us
