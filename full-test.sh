#!/usr/bin/env bash

# stop script on first error - "fail fast"
set -e
# print bash instructions to stdout
set -o xtrace

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

pushd "$SCRIPT_DIR"
git pull
pushd "./package"
dpkg-buildpackage -b -uc -us
popd
sudo dpkg -i *.deb
popd

time sudo TARGET_DIR=debs WORKING_DIR=tmp LOGS_DIR=logs /opt/build-bash-gawk-sed/bin/build-bash-gawk-sed.sh
